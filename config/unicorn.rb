root = "/home/sistemas/apps/tag_control/current"
working_directory root
pid "#{root}/tmp/pids/unicorn.tag_control.pid"
stderr_path "#{root}/log/unicorn_err.log"
stdout_path "#{root}/log/unicorn_out.log"

listen "/tmp/unicorn.tag_control.sock"
worker_processes 2
timeout 30

# Force the bundler gemfile environment variable to
# reference the capistrano "current" symlink
before_exec do |_|
  ENV["BUNDLE_GEMFILE"] = File.join(root, 'Gemfile')
end