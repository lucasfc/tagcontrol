Rails.application.routes.draw do
  resources :param_configs
  get 'consultas_publicas/receber_pergunta'

  post 'consultas_publicas/receber_participante'
  get 'consultas_publicas/configuration'

  get 'consultas_publicas/votacao'
  get 'consultas_publicas/registro'
  get 'consultas_publicas/devolucao'
  get 'consultas_publicas/receber_voto'
  get 'consultas_publicas/consultar_votos'
  get 'consultas_publicas/criar_area'
  get 'consultas_publicas/totalizar_votacao'

  resources :reply_people
  resources :replies
  resources :questions
  resources :tag_registers
  resources :person_tags
  resources :device_tags
  resources :areas
  resources :device_readers
  resources :people
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
