class CreateDeviceReaders < ActiveRecord::Migration[5.1]
  def change
    create_table :device_readers do |t|
      t.string :name
      t.string :code
      t.string :secrect
      t.string :description

      t.timestamps
    end
  end
end
