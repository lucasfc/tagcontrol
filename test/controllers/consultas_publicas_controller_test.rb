require 'test_helper'

class ConsultasPublicasControllerTest < ActionDispatch::IntegrationTest
  test "should get receber_pergunta" do
    get consultas_publicas_receber_pergunta_url
    assert_response :success
  end

  test "should get receber_participante" do
    get consultas_publicas_receber_participante_url
    assert_response :success
  end

  test "should get votacao" do
    get consultas_publicas_votacao_url
    assert_response :success
  end

end
