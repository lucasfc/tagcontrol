require 'test_helper'

class DeviceReadersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @device_reader = device_readers(:one)
  end

  test "should get index" do
    get device_readers_url, as: :json
    assert_response :success
  end

  test "should create device_reader" do
    assert_difference('DeviceReader.count') do
      post device_readers_url, params: { device_reader: { code: @device_reader.code, description: @device_reader.description, name: @device_reader.name, secrect: @device_reader.secrect } }, as: :json
    end

    assert_response 201
  end

  test "should show device_reader" do
    get device_reader_url(@device_reader), as: :json
    assert_response :success
  end

  test "should update device_reader" do
    patch device_reader_url(@device_reader), params: { device_reader: { code: @device_reader.code, description: @device_reader.description, name: @device_reader.name, secrect: @device_reader.secrect } }, as: :json
    assert_response 200
  end

  test "should destroy device_reader" do
    assert_difference('DeviceReader.count', -1) do
      delete device_reader_url(@device_reader), as: :json
    end

    assert_response 204
  end
end
