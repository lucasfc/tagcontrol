class TagRegister < ApplicationRecord
  belongs_to :person_tag
  belongs_to :device_reader, optional: true
  belongs_to :area

  before_save :send_hook

  def send_hook
    begin
      if area.event_id.present? and area.theme_id.present?
        path_enter= ParamConfig.find_by(namespace: "path_enter").value
        host= ParamConfig.find_by(namespace: "ppa_host").value
        url = "#{host}#{path_enter}"

        RestClient.post(url, {
            entradas:[
                theme_id: area.theme_id,
                event_id: area.event_id,
                cpf: person_tag.person.idnumber
            ]
        })
      end
    rescue
    end

  end
end
