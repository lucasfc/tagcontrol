class Person < ApplicationRecord

  validates :idnumber, uniqueness: true

  def self.create_by_ppa(params)
    
    person = find_or_create_by(idnumber: params[:cpf])
    person.update(
        name: params[:nome],
        idnumber: params[:cpf],
        email: params[:email],
        role: params[:cargo],
        date: params[:data],
        company: params[:empresa],
        mother: params[:mae],
        gender: params[:sexo]
    )

    person.sync_in_device(params[:tag])
  end

  def sync_in_device(code)
    device = DeviceTag.find_or_create_by(code: code)
    last_rel = PersonTag.where(active: true, device_tag: device)
    if last_rel.present?
      last_rel.update_all(active: false, inactivated_at: DateTime.now)
    end
    PersonTag.create(
                 active: true,
                 person: self,
                 device_tag: device
    )
  end
end
