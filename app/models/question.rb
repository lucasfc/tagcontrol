# Default param {"perguntas":[{"desafio":"Teste","event_id":1,"event_name":"Teste","theme_id":1,"theme":"Teste","vote_id":14},{"desafio":"Teste","event_id":1,"event_name":"Teste","theme_id":1,"theme":"Teste","vote_id":16},{"desafio":"Teste","event_id":1,"event_name":"Teste","theme_id":19,"theme":"Teste"}]}

class Question < ApplicationRecord

  has_many :replies
  has_many :reply_people, through: :replies

  def self.create_by_ppa(list)
    grouped = list.group_by{|e| Question.define_id(e) }
    grouped.map do |g, qs|
     q =  find_or_create_by(
         code: g,
         title: qs.first[:theme],
         category: qs.first[:tipo]
      )
      qs.map do |r|
        reply = Reply.find_or_create_by(
            code: r[:vote_id]
        )
        reply.update(
            question: q,
            name: r[:desafio]
        )
      end
    end
    grouped
  end

  def self.define_id(e)
    "E#{e[:event_id]}T#{e[:theme_id]}"
  end

  def self.count_votation(code)
    question = Question.find_by_code(code)
    question.reply_people.group_by(&:reply).map{|r,l| {resposta: r, votos: l.count}}
  end
  
end