class Area < ApplicationRecord

  def self.register(tag, area)
    area = Area.find_by_code(area)
    if area.blank?
      return {status: false, mensagem: "Essa área não está registrada"}
    end
    device = DeviceTag.find_by_code(tag)
    if device.blank?
      return {status: false, mensagem: "Este dispositivo não está cadastrado"}
    end
    pt = PersonTag.find_by(device_tag: device, active:true)

    if pt.blank?
      return {status: false, mensagem: "Este dispositivo não está ativado"}
    end

    if area.exit_register
      tr = TagRegister.find_by(area: area, person_tag: pt, exit_at: nil)
      if tr.present?
        tr.update(exit_at: DateTime.now)
        return {status: true, mensagem: "SAÍDA REGISTRADA COM SUCESSO", alert: true, person: pt.person}
      end
    end

    if area.limit.present?
      register_counter = TagRegister.where(area: area, person_tag: pt).count
      if register_counter >= area.limit
        return {status: false, mensagem: "Este dispositivo já se registrou #{register_counter} vez(es) e não pode ser registrado novamente"}
      else
        tr =TagRegister.create(
                       person_tag: pt,
                       area: area,
        )

        return {status: true, mensagem: "Entrada registrada com sucesso", alert: false, person: pt.person}
      end
    end
    tr =TagRegister.create(
        person_tag: pt,
        area: area,
    )

    return {status: true, mensagem: "Entrada registrada com sucesso", alert: false, person: pt.person}

  end
end
