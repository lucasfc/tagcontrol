class ReplyPerson < ApplicationRecord
  belongs_to :reply
  belongs_to :person

  def self.register(tag, vote_id)
    vote = Reply.find_by_code(vote_id)
    if vote.blank?
      return {status: false, mensagem: "Essa resposta não está registrada"}
    end
    device = DeviceTag.find_by_code(tag)
    if device.blank?
      return {status: false, mensagem: "Este dispositivo não está cadastrado"}
    end
    pt = PersonTag.find_by(device_tag: device, active:true)

    if pt.blank?
      return {status: false, mensagem: "Este dispositivo não está ativado"}
    end

    reply_people = vote.question.reply_people.where(person: pt.person)
    if reply_people.present?
      return {status: false, mensagem: "Este participante já votou nesta pergunta"}
    end

    create(reply: vote, person: pt.person)
    {status: true, mensagem: "Voto Computado Com Sucesso"}
  end
end
