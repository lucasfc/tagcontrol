class ReplyPeopleController < ApplicationController
  before_action :set_reply_person, only: [:show, :update, :destroy]

  # GET /reply_people
  def index
    @reply_people = ReplyPerson.all

    render json: @reply_people
  end

  # GET /reply_people/1
  def show
    render json: @reply_person
  end

  # POST /reply_people
  def create
    @reply_person = ReplyPerson.new(reply_person_params)

    if @reply_person.save
      render json: @reply_person, status: :created, location: @reply_person
    else
      render json: @reply_person.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /reply_people/1
  def update
    if @reply_person.update(reply_person_params)
      render json: @reply_person
    else
      render json: @reply_person.errors, status: :unprocessable_entity
    end
  end

  # DELETE /reply_people/1
  def destroy
    @reply_person.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_reply_person
      @reply_person = ReplyPerson.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def reply_person_params
      params.require(:reply_person).permit(:reply_id, :person_id)
    end
end
