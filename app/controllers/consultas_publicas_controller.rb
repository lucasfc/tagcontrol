class ConsultasPublicasController < ApplicationController
  def receber_pergunta
    begin
      host = ParamConfig.find_by_namespace("ppa_host").value
      if params[:plenaria].present?
        perguntas = JSON.parse(RestClient.get("#{host}/events/#{params[:event_id]}/retornar_perguntas_plenarias"), symbolize_names: true)[:perguntas]
      else
        perguntas = JSON.parse(RestClient.get("#{host}/events/#{params[:event_id]}/retornar_desafios?theme_id=#{params[:theme_id]}"), symbolize_names: true)[:perguntas]
      end
      Question.create_by_ppa(perguntas)
      render json: {status: :ok}
    rescue
      render json: {status: :error}
    end
  end

  def receber_participante
    #begin

      person = Person.create_by_ppa(params[:participante])
      render json: {status: :ok, person_tag: person}
    #rescue
    # render json: {status: :error}
    #end
  end

  def votacao
    question = Question.find_by_code(params[:votation])
    if question.present?
      render json: {status: true, votation: question, replies: question.replies }
    else
      render json: {status: false, mensagem: "Votação não cadastrada"}
    end
  end

  def receber_voto
    begin
      result = ReplyPerson.register(params[:tag], params[:vote])
      render json: result
    rescue
      render json: {status: false, mensagem: "500 - Algum problema está ocorrendo, comunique a TI"}
    end
  end

  def consultar_votos
    begin
      result = Reply.counter(params[:vote_id])
      render json: result
    rescue
      render json: {status: false, mensagem: "500 - Algum problema está ocorrendo, comunique a TI"}
    end
  end

  def configuration
    begin
      c = ParamConfig.find_or_create_by(namespace: params[:namespace])
      c.update(value: params[:value])


      render json: {status: :ok}
    rescue
      render json: {status: :error}
    end
  end

  def registro
    begin
      result = Area.register(params[:tag], params[:area])
      render json: result
    rescue
      render json: {status: false, mensagem: "500 - Algum problema está ocorrendo, comunique a TI"}
    end
  end

  def devolucao
    begin
      result = PersonTag.deactive(params[:tag])
      render json: result
    rescue
      render json: {status: false, mensagem: "500 - Algum problema está ocorrendo, comunique a TI"}
    end
  end

  def criar_area
    begin
      result = Area.create(JSON.parse(params[:area]))
      render json: result
    rescue
      render json: {status: false, mensagem: "500 - Algum problema está ocorrendo, comunique a TI"}
    end
  end

  def totalizar_votacao
    begin
      result = Question.count_votation(params[:code])
      render json: result
    rescue
      render json: {status: false, mensagem: "500 - Algum problema está ocorrendo, comunique a TI"}
    end
  end
end
